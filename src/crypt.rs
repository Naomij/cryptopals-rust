//! Bulk of the cryptographic functions


use crypto::aessafe::AesSafe128Encryptor;
use crypto::aessafe::AesSafe128Decryptor;
use crypto::symmetriccipher::BlockEncryptor;
use crypto::symmetriccipher::BlockDecryptor;
use std::collections::HashMap;
use std::str;
use std::iter;
use rand::prelude::*;

/// Byte-by-byte xor of two vectors
/// 
/// I should make it fail when the 2 vecs are not the same size
/// Right now it returns a dummy vec.
pub fn xorvec(v: Vec<u8>, w: Vec<u8> ) -> Vec<u8>
{
    if v.len()==w.len()
    {
        let mut res = Vec::new();
        for i in 0..v.len()
        {
            res.push(v[i]^w[i])
        }
        res
    } else
    {
        vec![0]
    }

}
/// Find the most likely keysize for a ECB
pub fn find_keysize(mys : &Vec<u8>) -> usize
{
    let mut d = 100.;
    let mut keysize = 0;
    for k in 6..40
    {
        let mut dtemp = 0.;
        for i in 0..6
        {
            dtemp += dhamming(&mys[i*k..(i+1)*k],&mys[(i+1)*k..(i+2)*k]) as f32;
        }
        if dtemp/(7.*k as f32)<d
        {
            d=dtemp/(7.*k as f32);
            keysize = k;
        }
    }
    println!("keysize is {}", keysize);
    keysize // 29
}

/// Score a plaintext on its englishness
/// 
/// Calculate the euclidian distance between mean english letter frequency and plaintext letter frequency
pub fn scorestr(s : &str) -> f32 // interdiction formelle pour Ambre de critiquer cette partie de code
{
    fn handle ( a : Option<&f32>) -> &f32
    {
        match a {
            Some(x) => x,
            None => &0.
        }
    }

    let mut freq = HashMap::new();
    let f : f32 = 1./(s.len() as f32);
    for c in s.chars() {
        let count = freq.entry(c).or_insert(0.);
        *count += f;
    }
    ((handle(freq.get(&'e'))+handle(freq.get(&'E'))-0.12702).powf(2.)+(handle(freq.get(&'t'))+handle(freq.get(&'T'))-0.09056).powf(2.)+
    (handle(freq.get(&'a'))+handle(freq.get(&'A'))-0.08167).powf(2.)+(handle(freq.get(&'o'))+handle(freq.get(&'O'))-0.07507).powf(2.)+
    (handle(freq.get(&'i'))+handle(freq.get(&'I'))-0.06966).powf(2.)+(handle(freq.get(&'n'))+handle(freq.get(&'N'))-0.06749).powf(2.)+
    (handle(freq.get(&'s'))+handle(freq.get(&'S'))-0.06327).powf(2.)+(handle(freq.get(&'h'))+handle(freq.get(&'H'))-0.06094).powf(2.)+
    (handle(freq.get(&'r'))+handle(freq.get(&'R'))-0.05987).powf(2.)+(handle(freq.get(&'d'))+handle(freq.get(&'D'))-0.04253).powf(2.)+
    (handle(freq.get(&'l'))+handle(freq.get(&'L'))-0.04253).powf(2.)+(handle(freq.get(&'c'))+handle(freq.get(&'C'))-0.02782).powf(2.)+
    (handle(freq.get(&'u'))+handle(freq.get(&'U'))-0.02758).powf(2.)+(handle(freq.get(&'m'))+handle(freq.get(&'M'))-0.02406).powf(2.)+
    (handle(freq.get(&'w'))+handle(freq.get(&'W'))-0.02360).powf(2.)+(handle(freq.get(&'f'))+handle(freq.get(&'F'))-0.02228).powf(2.)+
    (handle(freq.get(&'g'))+handle(freq.get(&'G'))-0.02015).powf(2.)+(handle(freq.get(&'y'))+handle(freq.get(&'Y'))-0.01974).powf(2.)+
    (handle(freq.get(&'p'))+handle(freq.get(&'P'))-0.01929).powf(2.)+(handle(freq.get(&'b'))+handle(freq.get(&'B'))-0.01492).powf(2.)+
    (handle(freq.get(&'v'))+handle(freq.get(&'V'))-0.00978).powf(2.)+(handle(freq.get(&'k'))+handle(freq.get(&'K'))-0.00772).powf(2.)+
    (handle(freq.get(&'j'))+handle(freq.get(&'J'))-0.00153).powf(2.)+(handle(freq.get(&'x'))+handle(freq.get(&'X'))-0.00150).powf(2.)+
    (handle(freq.get(&'q'))+handle(freq.get(&'Q'))-0.00095).powf(2.)+(handle(freq.get(&'z'))+handle(freq.get(&'Z'))-0.00074).powf(2.)+
    (handle(freq.get(&'_'))+handle(freq.get(&'-'))).powf(2.)+(handle(freq.get(&' '))-0.1918182).powf(2.)+
    (handle(freq.get(&'$'))+handle(freq.get(&'*'))).powf(2.)+(handle(freq.get(&'&'))+handle(freq.get(&'!'))-0.00074).powf(2.)).sqrt()
}

///Guesses which char has been xor'd against a Vec
/// 
/// Uses scorestr to find the most likely char.
pub fn guessxor1 (msg : Vec<u8>) -> char
{
    let mut min = 1.;
    let mut key = '\0';
    for i in 10u8..255u8
    {
        let mut res = Vec::new();
        for j in 0..msg.len()
        {
            if msg[j]^(i as u8)>30 && msg[j]^(i as u8)<126
            {
                res.push(msg[j]^(i as u8))
            }

        }
        let msg2 = match str::from_utf8(&res)
        {
            Ok(x) => x,
            Err(_) => "zzzzzzzzzzzzzzzzz"
        };
        if scorestr(msg2)<min && msg2.is_ascii()
        {
            println!("{:?} {} {}",scorestr(msg2), i, msg2);
            min = scorestr(msg2);
            key = i as char;
        }

    }
    key
}
/// Calculate the hamming distance between two `&[u8]`
pub fn dhamming(x: &[u8], y: &[u8]) -> u32
{
    x.iter().zip(y).fold(0, |a, (b, c)| a + (*b ^ *c).count_ones() as u32)
}
/// Pads a mutable vec reference using pcks#7
pub fn pkcs7( msg: &mut Vec<u8>, l: u16) -> Vec<u8>
{
    let p = (l - msg.len() as u16) as u8;
    for _i in 0..p
    {
        msg.push(p);
    }
    msg.to_vec()
}
/// Decrypts an ECB ciphertext
pub fn ecb_decrypt<'mys>(key: &[u8], mys: &'mys mut std::vec::Vec<u8>) -> Vec<u8>
{
    let cipher = AesSafe128Decryptor::new(key);
    let mut trueresult : Vec<u8> = vec!();
    let blocks = mys.chunks(16);
    let mut result : Vec<[u8;16]> = vec!();
    let mut i = 0;
    for b in blocks
    {
        result.push([0u8;16]);
        cipher.decrypt_block(b,&mut result[i]);
        i+=1;
    }
    for b in result.iter()
    {
        for j in 0..15
        {
            trueresult.push(b[j]);
        }
    }
    trueresult
}


/// Encrypts a 16 bytes-long &[u8] to a Vec<u8> with an &[u8] as key
pub fn ecb_encrypt_block(key: &[u8], mes: &[u8]) -> Vec<u8> 
{
    let cipher = AesSafe128Encryptor::new(key);
    let mut result = [0u8;16];
    cipher.encrypt_block(mes,&mut result);
    result.to_vec()
}
/// Encrypts a plaintext in ECB mode
/// 
/// # Example
/// 
/// ```
/// assert_eq!(hex::encode(crypt::ecb_encrypt(b"YELLOW SUBMARINE", b"Be gay, do crime"))="a9649522a448e01bb207789ad54af1d5");
/// ```
pub fn ecb_encrypt(key: &[u8], mes: &[u8]) -> Vec<u8> //crypt more than just 16
{
    let blocks = mes.chunks(key.len());
    let mut result : Vec<u8> = vec!();
    for b in blocks
    {
        result.append(&mut ecb_encrypt_block(key, &mut b.to_vec()));
    }
    result
}
/// Encrypts one block in CBC mode.
/// 
/// In practice, xor a 16 bytes-long &[u8] to an &[u8] and encrypts the result to a Vec<u8> with an &[u8] as key and xor it 

fn cbc_encrypt_block(key: &[u8], mes: &[u8], iv : &[u8]) -> Vec<u8>
{
    let res =  xorvec(mes.to_vec(), iv.to_vec());
    ecb_encrypt_block(key, &res)
}
/// Encrypts a plaintext in ECB mode
fn cbc_encrypt(key: &[u8], mes: &mut [u8], iv : &[u8]) -> Vec<u8>
{
    let mut blocks = mes.chunks_mut(iv.len());
    let mut prev =  blocks.next().unwrap();
    let mut result = cbc_encrypt_block(key, &mut prev.to_vec(), iv);
    for b in blocks
    {
        result.append(&mut cbc_encrypt_block(key, &mut b.to_vec(), prev));
        prev = b;
    }
    result
}

fn cbc_decrypt_block(key: &[u8],mys: &mut std::vec::Vec<u8>, prev: &[u8]) -> std::vec::Vec<u8>
{
    let res = ecb_decrypt(key, mys);
    xorvec(prev.to_vec(), res.to_vec())
}

pub fn cbc_decrypt(key : &[u8], mes :&mut std::vec::Vec<u8>, iv: &[u8]) -> std::vec::Vec<u8>
{
    let bsize = iv.len();
    let mut blocks = mes.chunks_mut(bsize);
    let mut prev =  blocks.next().unwrap();
    let mut result = cbc_decrypt_block(key, &mut prev.to_vec(), iv);
    for b in blocks
    {
        result.append(&mut cbc_decrypt_block(key, &mut b.to_vec(), prev));
        prev = b;

    }
    result

}

///Generate a random 16-byte key
fn randkey() -> [u8; 16]
{
    let mut data = [0u8; 16];
    rand::thread_rng().fill_bytes(&mut data);
    data
}

pub fn encryption_oracle(plain : &mut Vec<u8>) -> Vec<u8>
{
    let key = randkey();
    let len : u8 = thread_rng().gen_range(5,10);
    let mut plain2 : Vec<u8> = iter::repeat(()).map(|()| thread_rng().gen()).take(len as usize).collect();
    let mut aft : Vec<u8> = iter::repeat(()).map(|()| thread_rng().gen()).take(len as usize).collect();
    plain2.append(plain);
    plain2.append(&mut aft);
    let len2 = (plain2.len()+(16-plain2.len()%16)) as u16;
    pkcs7(&mut plain2, len2) ; //(plain2.len()+(16-plain2.len()%16)) as u8)
    if thread_rng().gen()
    {
        println!("ECB");
        ecb_encrypt(&key,&mut plain2)

    }else{
        println!("CBC");
        let iv = randkey();
       cbc_decrypt(&key,&mut plain2,&iv)
    }
}

/// Black box used in challenge 12
pub fn ch12helper(plain : &mut Vec<u8>) -> Vec<u8>
{
    let key = b"randkey();ABCDEF"; // la clé pourrait être globale et aléatoire, mais cette approche est isomorphe
    let thestring = base64::decode("Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK").unwrap();
    plain.append(&mut thestring.to_vec());
    let len2 = (plain.len()+(16-plain.len()%16)) as u16;
    pkcs7(plain, len2) ; //(plain2.len()+(16-plain2.len()%16)) as u8)
    ecb_encrypt(key,plain)

    
}

pub fn validate_padding(plain : &mut [u8]) -> Result<bool, bool>
{
    plain.reverse();
    let mut count = 0u8;
    let mut iterator = plain.iter();
    let letter = iterator.next().unwrap();
    for i in iterator
    {
        if i==letter
        {
            count+=1;
        }
    }
    if letter == &count
    {
        Ok(true)
    }
    else {
        Err(false)
    }
}

pub struct Mersennetwister
{
    mt : [u32 ; 624],
    w : u32,
    index : usize,
    n : usize,
    m : usize,
    f : u32,
    a : u32,
    u : u32,
    d : u32,
    s : u32,
    b : u32,
    t : u32,
    c : u32,
    l : u32,
}
impl Mersennetwister
{
    pub fn init_mt(seed : u32) -> Mersennetwister
    {
        let mut a = Mersennetwister{
            mt:[0u32 ; 624],
            n:624,
            w:32,
            m:397,
            f:1812433253,
            a:0x9908B0DF,
            u:11,
            d:0xFFFFFFFF,
            s:7,
            b:0x9D2C5680,
            t:15,
            c:0xEFC60000,
            l:18,
            index : 624,
        };
        a.mt[0]=seed;
        for i in 1..(a.n) as usize {
            a.mt[i] = (((a.f as u64 *(a.mt[i-1] as u64^(a.mt[i-1] as u64>>(a.w-2))) + i as u64) as u64) & 0xFFFFFFFF) as u32;
        }
        a
    }
    fn twist(&mut self)
    {
        for i in 0..(self.n) {
            let x = (self.mt[i] & 0x80000000) + (self.mt[(i+1) % self.n] & 0x7fffffff);
            let mut x_a = x >>1;
            if x%2!=0 {
                x_a = x_a ^ self.a;
            }
            self.mt[i] = self.mt[(i+self.m) % self.n] ^ x_a

        }
        self.index = 0;
    }
    pub fn extract_number(&mut self) -> i32
    {
        if self.index >= self.n
        {
            self.twist();
        }
        let mut y = self.mt[self.index];
        y = y ^ ((y>>self.u) & self.d);
        y = y ^ ((y<<self.s) & self.b);
        y = y ^ ((y<<self.t) & self.c);
        y = y ^ (y>>self.l);
        self.index +=1;
        y as i32
    }
}
