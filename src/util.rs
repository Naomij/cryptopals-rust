//! Some utilitary functions
//! 



pub fn keqvparse(entry : &str) //pas sûre de quoi faire 
{
    let list = entry.split("&");
    println!("{:?}", list.collect::<Vec<&str>>());
}

/// Create a profile String using a str slice as entry
/// 
/// 
/// Generates a random u8 uid. "&" and "=" are eaten by the function
/// 
///  # Examples
/// ```
/// profile_for("test@gmail.com"); // "email=test@gmail.com&uid=25&role=user"
/// ```
pub fn profile_for(entry : &str) -> String
{
    let email = entry.replace("&", "").replace("=", ""); //miam
    let uid : u8 =  12; //thread_rng().gen();
    format!("email={}&uid={}&role=user",email,uid)
}