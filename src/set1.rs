//! The first set
//! 
//! [Link](https://cryptopals.com/sets/1)

extern crate hex;
extern crate base64;
extern crate crypto;
extern crate rand;
extern crate itertools;
use std::str;
use std::io::prelude::*;
use std::fs::File;
use crate::crypt;

/// Challenge 1
/// 
/// 
/// We use the hex crate to decode a string to a `Vec<u8>` and then we use the base64 crate to print it to base64
/// 
/// [Link](https://cryptopals.com/sets/1/challenges/1)
pub fn ch1()
{
    let msg = hex::decode("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d").unwrap();
    println!("{:?}", base64::encode(&msg));
}
/// Challenge 2
/// 
/// Straightforward, we use `crypt::xorvec`
/// 
/// [Link](https://cryptopals.com/sets/1/challenges/2)
pub fn ch2()
{
    let msg = hex::decode("1c0111001f010100061a024b53535009181c").unwrap();
    let fxor = hex::decode("686974207468652062756c6c277320657965").unwrap();
    let rez = crypt::xorvec(msg,fxor);
    println!("{}", hex::encode(rez));
}
/// Challenge 3
/// 
/// [Link](https://cryptopals.com/sets/1/challenges/3)
pub fn ch3(m : &str)
{
    //ch3("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")
    let msg = hex::decode(m).unwrap();
    println!("{:?}", crypt::guessxor1(msg));

}

/// Challenge 5
/// 
/// Straighforward, we iterate on a byte array and we use `^` to xor two u8
///
/// [Link](https://cryptopals.com/sets/1/challenges/5)
pub fn ch5()
{
    let msg = String::from("Burning 'em, if you ain't quick and nimble \nI go crazy when I hear a cymbal").into_bytes(); //b""
    let mut res = Vec::new();
    let key = ['I' as u8, 'C' as u8, 'E' as u8];
    for i in 0..msg.len()
    {
        res.push(msg[i] ^ key[i%3])
    }
    println!("{}", hex::encode(res));
}

/// Challenge 6
/// 
/// First real challenge. We try to follow the algorithm described.
/// 
/// I need to clean the code :'(
/// 
/// [Link](https://cryptopals.com/sets/1/challenges/6)
pub fn ch6() // some cleanup to do, but okayish
{
    let mut msg = String::new();
    let mut file = File::open("6.txt").expect("Unable to open the file");
    file.read_to_string(&mut msg).expect("Unable to read the file");
    let mys = base64::decode(&msg.replace("\n","")).unwrap();

    let keysize = crypt::find_keysize(&mys);
    let mut result = Vec::new();
    print!("key is : ");
    for offset in 0..keysize
    {
        let mut msgend =  String::from("");
        let mut vec1 = Vec::new();
        for i in 0..(mys.len()/keysize) as usize
        {
            vec1.push(mys[offset+i*keysize])
        }

        let mut min = 1.;
        let mut key ='\0';
        for i in 10u8..255u8
        {
            let mut res = Vec::new();
            for j in 0..vec1.len()
            {
                if (vec1[j]^(i as u8)>30 || vec1[j]^(i as u8)==10)&& vec1[j]^(i as u8)<126
                {
                    res.push(vec1[j]^(i as u8))
                }

            }
            let msg2 = match str::from_utf8(&res)
            {
                Ok(x) => x,
                Err(_) => "zzzzzzzzzzzzzzzzz"
            };
            if crypt::scorestr(msg2)<min && msg2.is_ascii()
            {
                min = crypt::scorestr(msg2);
                msgend = String::from(msg2);
                key = i as char;
            }
        }
        print!("{}", key);
        result.push(msgend);
    }
    let mut resultchar = Vec::new();
    println!("\n-------------");
    for i in result.iter()
    {
        resultchar.push(i.chars());
    }
    for _j in 0..70
    {
        for i in 0..resultchar.len()
        {
            print!("{}", resultchar[i].next().unwrap());
        }
    }
}
/// Challenge 7
/// 
/// Again an easy one. We use rust-crypto's implemetation of AES, using only the `decrypt_block` and `encrypt_block` traits.
/// 
/// Everything else is managed using homemade code located inside the crypt module.
/// 
/// [Link](https://cryptopals.com/sets/1/challenges/7)
pub fn ch7()
{
    let mut msg = String::new();
    let mut file = File::open("7.txt").expect("Unable to open the file");
    file.read_to_string(&mut msg).expect("Unable to read the file");
    let mut mys = base64::decode(&msg.replace("\n","")).unwrap();
    let key = b"YELLOW SUBMARINE";
    println!("{}",str::from_utf8(&crypt::ecb_decrypt(key,&mut mys)).unwrap());
}

/// Challenge 8
/// 
/// Detects ECB encrypting by checking for a collision. Need to be run using the release option, otherwise the program hangs.
/// 
/// [Link](https://cryptopals.com/sets/1/challenges/8)
pub fn ch8() //is it done ? -- needs release
{
    let mut msg = String::new();
    let mut file = File::open("8.txt").expect("Unable to open the file");
    file.read_to_string(&mut msg).expect("Unable to read the file");
    let nblines = msg.matches('\n').count();
    let mys = hex::decode(&msg.replace("\n","")).unwrap();
    for i in 0..(mys.len()-16)
    {
        for j in i+1..(mys.len()-16)
        {
            if mys[i..i+16]==mys[j..j+16]
            {
                println!("Possible ECB @ {},{}, block number {}",i,j,i/nblines);
            }
        }
    }
}