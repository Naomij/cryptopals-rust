Doc is [here](https://naomij.gitlab.io/cryptopals-rust/doc/cryptopals_rust/index.html)

To run the program use `cargo run #challenge` where #challenge is the number of the challenge you wish to run

To update the doc use `cargo doc --no-deps --document-private-items --target-dir doc`

To see how stupid I am use `cargo clippy`